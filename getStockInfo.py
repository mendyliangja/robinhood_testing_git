import config
import robin_stocks as rs
import csv

symbol = 'PDD'



rs.login(config.USERNAME, config.PASSWORD)
# stock_historial_data = rs.stocks.get_stock_historicals(symbol,interval='day',span='3month')
# print(stock_historial_data[0])

# # This part is to get history prices
# stock_prices = []
# stock_historial_data = rs.stocks.get_stock_historicals(symbol,interval='day',span='3month')
# #print(stock_historial_data[0])
# for item in stock_historial_data:
#     price = (item['open_price'])
#     stock_prices.append(price)

# filename = symbol+'_price_history.csv'
# with open(filename, 'w', newline='') as f:
#     for i in range(len(stock_prices)):
#         f.write("\"%s\""%(stock_prices[i]))
#         f.write('\n')


#This part is to get daily variance in max decreasing percentage
Dec_Rates = []
stock_historial_data = rs.stocks.get_stock_historicals(symbol,interval='day',span='3month')
print(stock_historial_data[0])

for item in stock_historial_data:
    high_price = float(item['high_price'])
    low_price = float(item['low_price'])
    max_decreasing_rate = (low_price-high_price)/high_price
    Dec_Rates.append(max_decreasing_rate)

filename = symbol+'_max_dec_history.csv'
with open(filename, 'w', newline='') as f:
    for i in range(len(Dec_Rates)):
        f.write("\"%s\""%(Dec_Rates[i]))
        f.write('\n')


rs.authentication.logout()