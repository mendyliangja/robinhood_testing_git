import config
import robin_stocks as rs
from datetime import datetime
import time
HYS_highest_price = 90.0
HYS_threshold_price = 100.0
HYS_threshold_decreasing_percentage = 0.05


rs.login(config.USERNAME, config.PASSWORD)

'''
每个 5 秒。
'''
def timedTask():
    global HYS_highest_price, HYS_threshold_price, HYS_threshold_decreasing_percentage
    while True:
        print(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        HYS_latest_price = rs.stocks.get_latest_price('HYS')
        HYS_latest_price = float(HYS_latest_price[0])
        print('=====HYS latest price=====')
        print(HYS_latest_price)

        if HYS_latest_price > HYS_highest_price:
            HYS_highest_price = HYS_latest_price
            print("====HYS highest price changed====")
            print(HYS_highest_price)
        else:
            print("====no change in HYS highest price====")

        HYS_threshold_price = (1-HYS_threshold_decreasing_percentage)*HYS_highest_price
        print('=====HYS threshold price=====')
        print(HYS_threshold_price)

        if HYS_latest_price < HYS_threshold_price:
            print('====sell HYS at price====')
            print(HYS_latest_price)
            # get stock quantity and sell
            my_stocks = rs.build_holdings()
            HYS_quantity = int(float(my_stocks['HYS']['quantity']))
            sell_order_HYS = rs.orders.order_sell_limit('HYS',HYS_quantity,HYS_latest_price)
            if 'ref_id' in sell_order_HYS:
                print('====sell order placed====')
                break
            else:
                print('ATTENTION!!!!!!!!!!!!!!!Error in placing sell order!')
                break
        else:
            print('no sell since price is higher than threshold')
        time.sleep(5)

if __name__ == '__main__':
    timedTask()

rs.authentication.logout()