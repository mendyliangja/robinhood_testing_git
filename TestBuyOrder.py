import config
import robin_stocks as rs

symbol = 'PDD'
stock_quantity = 1
limit_price = 1000.51
amountInDollars = 1.00

rs.login(config.USERNAME, config.PASSWORD)


#my_stocks = rs.build_holdings()

buy_type = input("input 'mar' for market buy; input 'lim' for limit buy; input 'fra' for fractional buy: ")

if buy_type == 'mar':
    print('please confirm that we will buy %d number of stock %s at market price'%(stock_quantity,symbol))
    confirmation = input('input "y" to continue: ')
    if confirmation == 'y':
        print("test purpose: placed market buy order")
        # buy_order = rs.orders.order_buy_market(symbol, stock_quantity, extendedHours=True)
        # if 'ref_id' in buy_order:
        #     print('====market buy order placed====')
        # else:
        #     print("!!!ATTENTION!!!===market buy order failed====")
    else:
        print("you did not confirm buy order, please start again")
elif buy_type == 'lim':
    print('please confirm that we will buy %d number of stock %s at limit price %f'%(stock_quantity,symbol,limit_price))
    confirmation = input('input "y" to continue: ')
    if confirmation == 'y':
        print("test purpose: placed limit buy order")
        # buy_order = rs.orders.order_buy_limit(symbol, stock_quantity, limit_price, extendedHours=True)
        # if 'ref_id' in buy_order:
        #     print('====limit buy order placed====')
        # else:
        #     print("!!!ATTENTION!!!===limit buy order failed====")
    else:
        print("you did not confirm buy order, please start again")
elif buy_type == 'fra':
    print('please confirm that we will buy %f amountInDollars of stock %s'%(amountInDollars,symbol))
    confirmation = input('input "y" to continue: ')
    if confirmation == 'y':
        print("test purpose: placed fractional buy order")
        # buy_order = rs.orders.order_buy_fractional_by_price(symbol, amountInDollars, extendedHours=True)
        # if 'ref_id' in buy_order:
        #     print('====fractional buy order placed====')
        # else:
        #     print("!!!ATTENTION!!!===fractional buy order failed====")
    else:
        print("you did not confirm buy order, please start again")
else:
    print("the input for buy type is not recognized, please start over")
    



# buy_order_id = buy_order['id']
# print('buy order id: ',buy_order_id)

# sell_order_cancel_result = rs.orders.cancel_stock_order(sell_order_HYS_id)
# print('======cancel sell order========')
# print(sell_order_cancel_result)









rs.authentication.logout()