import pandas as pd
import numpy as np
#import time
#import matplotlib.pyplot as plt

last_diff = 0.00
short_len = 50
long_len = 500
short_list = []
long_list = []
DollarAmount = 1000.0
Quantity = 0.0
filename = 'crossTest2.csv'
#filename = '2020-07-10_Recorded_stock_prices.csv'

data = pd.read_csv(filename)
stock_price = data['TSLA']
stock_price = stock_price.tolist()

#make an order decision based on golden cross or death cross
def cross_decision(last_diff,diff_ma):
    if last_diff < 0 and diff_ma >0:
        print("buy")
        return 1
    elif last_diff >0 and diff_ma <0:
        print("sell")
        return 2
    else:
        #print("no action")
        return 0

for i in range(len(stock_price)):
    if i < short_len:
        short_list.append(stock_price[i])
        long_list.append(stock_price[i])
    elif i < long_len:
        short_list.pop(0)
        short_list.append(stock_price[i])
        long_list.append(stock_price[i])
    else:
        short_list.pop(0)
        long_list.pop(0)
        short_list.append(stock_price[i])
        long_list.append(stock_price[i])
    if len(short_list) > short_len or len(long_list) > long_len:
        print("ma lists length error")
        print("the length of short list is %d"%len(short_list))
        print("the length of long list is %d"%len(long_list))
        print("i is %d"%i)
        break
    ma_short = np.mean(short_list)
    ma_long = np.mean(long_list)
    diff_ma = ma_short - ma_long
    #print("diff_ma is %f"%diff_ma)
    order_type = cross_decision(last_diff,diff_ma)
    last_diff = diff_ma


    if order_type == 1:
        if DollarAmount > 0.0:
            print("buy with dollar amount %f at price %f"%(DollarAmount, stock_price[i]))
            Quantity += DollarAmount/stock_price[i]
            DollarAmount = 0
    elif order_type == 2:
        if Quantity > 0:
            print("sell quantity %f at price %f"%(Quantity, stock_price[i]))
            DollarAmount += Quantity*stock_price[i]
            Quantity = 0

final_total = DollarAmount + Quantity*stock_price[-1]
default_total = 1000*(stock_price[-1]/stock_price[0])
print("==========Final state==========")
print("DollarAmount is %f, Quantity is %f"%(DollarAmount, Quantity))
print("Final total is %f"%final_total)
print("Default between close and open price total is: %f"%default_total)
# for i in range(2):
#     a.append(stock_price[0])
#     print(len(a))
# print(a)

#print(type(stock_price))
# test = stock_price[0] + stock_price[1]
# print(test)
# print(type(test))
# print(len(stock_price))

