import config
import robin_stocks as rs
from datetime import datetime
import time
import csv

symbol = 'PDD'
filename_hp = '.\\hp\\'+symbol+'_highest_price.csv'
stock_highest_price = 93.8
stock_threshold_price = 100.0
stock_threshold_decreasing_percentage = 0.04

#At start read highest price from file under .\hp\
with open(filename_hp) as f:
    reader = csv.reader(f)
    hp_list = list(reader)
    #print(hp_list[0][0])
    stock_highest_price = float(hp_list[0][0])
    
print("read from file, %s highest price is %f"%(symbol, stock_highest_price))

rs.login(config.USERNAME, config.PASSWORD)

'''
每个 5 秒。
'''
def timedTask():
    global stock_highest_price, stock_threshold_price, stock_threshold_decreasing_percentage
    while True:
        print(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        stock_latest_price = rs.stocks.get_latest_price(symbol)
        stock_latest_price = float(stock_latest_price[0])
        print('=====stock latest price=====')
        print(stock_latest_price)

        if stock_latest_price > stock_highest_price:
            stock_highest_price = stock_latest_price
            print("====stock highest price changed====")
            print(stock_highest_price)
            #write highest price to file if changed
            with open(filename_hp, 'w', newline='') as f:
                f.write("%s"%str(stock_highest_price))
        else:
            print("====no change in stock highest price====")
            print(stock_highest_price)

        stock_threshold_price = (1-stock_threshold_decreasing_percentage)*stock_highest_price
        print('=====stock threshold price=====')
        print(stock_threshold_price)

        if stock_latest_price < stock_threshold_price:
            print('====sell stock at price====')
            print(stock_latest_price)
            # get stock quantity and sell
            my_stocks = rs.build_holdings()
            stock_quantity = int(float(my_stocks[symbol]['quantity']))
            sell_order_stock = rs.orders.order_sell_limit(symbol,stock_quantity,stock_latest_price,extendedHours=True)
            if 'ref_id' in sell_order_stock:
                print('====sell order placed====')
                break
            else:
                print('ATTENTION!!!!!!!!!!!!!!!Error in placing sell order!')
                break
        else:
            print('no sell since price is higher than threshold')
        time.sleep(5)

if __name__ == '__main__':
    timedTask()

rs.authentication.logout()